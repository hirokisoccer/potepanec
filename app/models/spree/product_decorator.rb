Spree::Product.class_eval do
  MAX_RELATED_PRODUCTS = 4
  scope :related_products, -> (product) do
    includes_taxons.
      joins(:taxons).
      where(spree_taxons: {id:product.taxons.ids}).
      except_myself(product).
      distinct.limit(MAX_RELATED_PRODUCTS)
  end

  MAX_LATEST_PRODUCTS = 8
  scope :picking_latest_products, -> do
    includes_taxons.
      only_show_current_products.
      order(available_on: :desc).
      limit(MAX_LATEST_PRODUCTS)
  end

  scope :except_myself, -> (product) { where.not(id: product.id) }
  scope :includes_taxons, -> { includes(:taxons, [master: [:default_price, :images]]) }
  scope :only_show_current_products, -> { where('available_on < ?', Time.zone.now) }

end
