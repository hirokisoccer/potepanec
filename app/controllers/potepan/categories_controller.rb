class Potepan::CategoriesController < ApplicationController
 def show
   @taxon = Spree::Taxon.find(params[:id])
   @taxonomies = Spree::Taxonomy.all.includes(:taxons)
   @detailed_products = @taxon.products.includes(master: [:images, :default_price])
 end
end
