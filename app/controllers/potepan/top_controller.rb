class Potepan::TopController < ApplicationController
  def index
    @latest_products = Spree::Product.picking_latest_products
  end
end
