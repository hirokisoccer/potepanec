require 'rails_helper'

RSpec.describe Potepan::TopController, type: :controller do

  describe "GET #index" do
    before { get :index }

    it 'properly status code return as OK' do
      expect(response.status).to eq 200
    end

    it "renders the index template" do
      expect(response).to render_template :index
    end
  end

  describe "@latest_products" do
    before { get :index }

    context 'when more than the max_latest_products number(8) of products exist within 1 hour' do
      let(:max_latest_products) { 8 }
      let!(:new_products) { create_list(:product, 9, available_on: 1.hour.ago) }

      it "has the same number(8) of max_latest_products counts" do
        expect(assigns(:latest_products).size).to eq max_latest_products
      end
    end

    context 'when less than the max_latest_products number(8) of products exist within 1 hour' do
      let(:max_latest_products) { 2 }
      let!(:new_products) { create_list(:product, 2, available_on: 1.hour.ago) }

      it "has the same number(2) of max_latest_products counts" do
        expect(assigns(:latest_products).size).to eq max_latest_products
      end
    end

    context 'when product_release_tomorrow exists' do
      let(:product_release_tomorrow) { create(:product, available_on: 1.days.from_now) }

      it "doesn't include @latest_products" do
        expect(assigns(:latest_products)).not_to include product_release_tomorrow
      end
    end
  end

  describe "make sure @latest_products is descending" do
    before { get :index }
    let(:new_products) {
      (1..3).map { |n| create(:product, available_on: n.days.ago) }
    }

    it 'displays correct new arrivals order' do
      expect(assigns(:latest_products)).to match new_products
    end
  end

end
