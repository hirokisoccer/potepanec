require "rails_helper"

RSpec.describe Potepan::ProductsController, type: :controller do
  describe 'GET #show' do
    let(:product) { create(:product) }
    before { get :show, params: { id: product.id }}

    it 'can been seen show template' do
      expect(response).to render_template :show
    end

    it 'properly status code return as 200(Oaky)' do
      expect(response.status).to eq 200
    end

    it "can be accessible to @product" do
      expect(assigns(:product)).to eq product
    end
  end

  describe'@related_products' do
    let(:category_taxonomy) { create(:taxonomy, name: 'Categories') }
    let(:mugs_taxon)        { create(:taxon, name: 'Mugs', taxonomy: category_taxonomy) }
    let(:product) do
      create(:product) do |product|
        product.taxons << mugs_taxon
      end
    end
    before { get :show, params: { id: product.id }}
    subject { assigns(:related_products).count }

    context "渡された関連商品が定数（4）より多い場合" do
      let(:max_related_products) { 4 }
      let!(:product_in_bag_category) do
        create_list(:product, 5) do |product|
          product.taxons << mugs_taxon
        end
      end
      it { is_expected.to eq max_related_products }
    end

    context "渡された関連商品が定数（4）未満の場合" do
      let(:related_product_counts) { 3 }
      let!(:products_in_bag_category) do
        create_list(:product, related_product_counts) do |product|
          product.taxons << mugs_taxon
        end
      end
      it { is_expected.to eq related_product_counts }
    end

    context "渡された関連商品の定数（4）が同じ場合" do
      let(:related_product_counts) { 4 }
      let!(:products_in_bag_category) do
        create_list(:product, related_product_counts) do |product|
          product.taxons << mugs_taxon
        end
      end
      it { is_expected.to eq related_product_counts }
    end
  end

end
