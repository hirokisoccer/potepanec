require 'rails_helper'

RSpec.describe Potepan::CategoriesController, type: :controller do
  describe 'GET #show'  do
    let(:taxon) {create(:taxon)}
    before { get :show, params: { id: taxon.id }}

    it 'can been seen show template' do
      expect(response).to render_template :show
    end

    it 'properly status code return as 200(Oaky)' do
      expect(response.status).to eq 200
    end

    it "can be accessible to @product" do
      expect(assigns(:taxon)).to eq taxon
    end

  end
end
